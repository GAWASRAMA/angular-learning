import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, pipe, Subject, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { User } from './user.model';

export interface AuthResData {
  idToken: string,
  email: string,
  refreshToken: string,
  expiresIn: string,
  localId: string,
  registered?:boolean
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = new BehaviorSubject<User>(null);
  private tokenExpirationTimer: any;
  constructor(private http: HttpClient,private router: Router) { }

  signUp(email: string, password: string): any {
    return this.http.post<AuthResData>('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyD8UJdlp836faHqB-XJ7BZlaIEFlTQGVIs',
      {
        email: email,
        password: password,
        returnSecureToken: true
      }
    )
    .pipe(
      catchError(errorRes=>{
        let errroMessage = 'An error occurred!';
        if(errorRes.error || errorRes.error.error){
         return throwError(errroMessage);
        }
        switch(errorRes.error.error.message){
          case "EMAIL_EXISTS":
            errroMessage='The email address is already in use by another account';
        }
        return throwError(errroMessage);
      }),tap(resData=>{
        this.handleAuthentication(
          resData.email,
          resData.localId,
          resData.idToken,
          +resData.expiresIn
        );
      })
    );
  }

  private handleAuthentication(
    email: string,
    userId: string,
    token: string,
    expiresIn: number
  ) {
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    const user = new User(email, userId, token, expirationDate);
    this.user.next(user);
    localStorage.setItem('userData', JSON.stringify(user));
  }

  logIn(email:string, password:string){
return this.http.post<AuthResData>('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyD8UJdlp836faHqB-XJ7BZlaIEFlTQGVIs',
{
  email: email,
  password: password,
  returnSecureToken: true
})
.pipe(
  catchError(errorRes=>{
    let errroMessage = 'An error occurred!';
    if(errorRes.error || errorRes.error.error){
     return throwError(errroMessage);
    }
    switch(errorRes.error.error.message){
      case "EMAIL_EXISTS":
        errroMessage='The email address is already in use by another account';
    }
    return throwError(errroMessage);
  }),
  tap(resData => {
    this.handleAuthentication(
      resData.email,
      resData.localId,
      resData.idToken,
      +resData.expiresIn
    );
    this.router.navigate(['/assingment']);
  })
);
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/log-in']);
    localStorage.removeItem('userData');
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
  }
}
