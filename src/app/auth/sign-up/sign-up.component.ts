import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService,AuthResData } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
isLoading = false;
isLoginMode = true;
  error: string;
  constructor(private service:AuthService,private router: Router,) { }

  ngOnInit(): void {
  }
  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }
  
  onSubmit(form:NgForm): void{
    let authObs: Observable<AuthResData>;
this.isLoading=true;
const email = form.value.email;
const password = form.value.password;
if (this.isLoginMode) {
  authObs = this.service.logIn(email, password);
} else {
  authObs = this.service.signUp(email, password);
}

authObs.subscribe(
  resData => {
    console.log(resData);
    this.isLoginMode = true;
    this.isLoading = false;
  },
  errorMessage => {
    console.log(errorMessage);
    this.error = errorMessage;
    this.isLoading = false;
  });
form.reset();
  }
}
