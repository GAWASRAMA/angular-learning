import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
const appRoutes: Routes = [
  {
    path: "login",
    loadChildren: () =>
      import("./auth/auth.module").then(m => m.AuthModule)
  },
  {
    path: "assingment",
    loadChildren: () =>
      import("./assignments/assignment.module").then(
        m => m.AssingmentModule
      )
  },
  { path: '**', redirectTo: 'login' }
];
  
  @NgModule({
    imports:  [
        RouterModule.forRoot(appRoutes)
    ],
    exports:[RouterModule]
  })
export class AppRoutingModule{

}