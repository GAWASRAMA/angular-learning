import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './auth/auth-guard.service';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { LoadingSpinnerComponent } from './shared/loadding-spinner/loading-spinner/loading-spinner.component';
import { AuthInterceptorService } from './auth/intercaptor.service';
import { AuthService } from './auth/auth.service';
import { SharedModule } from './shared/shared.module';
import { AssingmentModule } from './assignments/assignment.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [AuthService,AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
