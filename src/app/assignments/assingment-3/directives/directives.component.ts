import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.scss']
})
export class DirectivesComponent implements OnInit {
showData = false;
data = [];
  constructor() { }

  ngOnInit(): void {
  }
  toggleData(): void{
    this.showData = !this.showData;
    this.data.push(this.data.length + 1)
  }
}
