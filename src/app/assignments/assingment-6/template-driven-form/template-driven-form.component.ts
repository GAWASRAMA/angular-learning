import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { UserManageService } from '../../services/user-manage.service';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent implements OnInit {
  subData = ['Basic', 'Advanced', 'Pro'];
  status = ['Stable', 'Critical', 'Finished'];
  formData = true;
  data = true;
  showReactiveFormView = true;
  showTemplateDirvenFormView = true;
  userData=[];
  projectData=[];
  @ViewChild('userForm') userForm: NgForm;
  projctForm: FormGroup;
  constructor(private service:UserManageService) { }

  ngOnInit(): void {
    this.projctForm = new FormGroup({
      projectName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      status: new FormControl('Stable')
    });
  }

  onReactiveFormView(): void {
    this.showReactiveFormView = true;
    this.showTemplateDirvenFormView = false;
    this.formData = true;
  }

  onTdFormView(): void {
    this.showReactiveFormView = false;
    this.showTemplateDirvenFormView = true;
    this.data = true;
  }

  onSubmit(): void {
    this.userData.push( this.userForm.value); 
    this.showReactiveFormView = true;
    this.formData = false;
    this.userForm.reset();
  }

  onReactiveFormSubmit(): void {
    this.projectData.push( this.projctForm.value); 
    this.showTemplateDirvenFormView = true;
    this.data = false;
    this.projctForm.reset();

  }

}
