import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserManageService {
vegetable = [];
  constructor() { }
 getAllData(): any{
   return this.vegetable;
 }

  addToOrder(item:any): void{
    console.log(item);
    var index = this.vegetable.findIndex(x => x.name == item.name);
    console.log(index)
    if(index >= 0){
      this.vegetable[index].Quantity = this.vegetable[index].Quantity +1;
    }
    else{
      this.vegetable.push({
        imagePath:item.imagePath,
        name:item.name,
        Quantity:1,
        amount:item.amount
      });
    }
    console.log(this.vegetable.length);
  }

  removeProduct(name:string){
    var index = this.vegetable.findIndex(x => x.name == name);
    if(index >= 0){
      this.vegetable.splice(index,1);
    }
  }
}
