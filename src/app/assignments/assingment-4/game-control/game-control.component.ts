import { Component, OnInit,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.scss']
})
export class GameControlComponent implements OnInit {
  @Output() intervalFired= new EventEmitter<number>();
  @Output() clearData= new EventEmitter<number>();
interval;
lastNumber = 0;
  constructor() { }

  ngOnInit(): void {
  }

  onStartGame(): void {
this.interval = setInterval(()=>{
this.intervalFired.emit(this.lastNumber + 1);
this.lastNumber++;
},1000);
  }

  onPauseGame(): void{
    clearInterval(this.interval);
  }

  onClear(): void{
    this.clearData.emit(this.lastNumber=0);
    clearInterval(this.interval);
  }
}
