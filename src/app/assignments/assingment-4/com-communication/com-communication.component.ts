import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-com-communication',
  templateUrl: './com-communication.component.html',
  styleUrls: ['./com-communication.component.scss']
})
export class ComCommunicationComponent implements OnInit {
  oddNumbers:number[]=[];
  evenNumbers:number[]=[];
  onIntervalFired(firedNumber:number): void{
   (firedNumber % 2 === 0) ? this.evenNumbers.push(firedNumber) :this.oddNumbers.push(firedNumber);
  }
  onDataClear(): void{
    this.oddNumbers =[];
    this.evenNumbers = [];
  }
  ngOnInit(): void {
  }

}
