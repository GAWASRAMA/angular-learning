import { Component, OnInit } from '@angular/core';
import { UserManageService } from '../../services/user-manage.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
items = [];
  constructor(private service:UserManageService) { }

  ngOnInit(): void {
    this.items = this.service.getAllData();
  }

  removeFromCart(name:string){
    this.service.removeProduct(name);
  }

}
