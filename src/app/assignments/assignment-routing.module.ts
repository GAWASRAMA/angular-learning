import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SignUpComponent } from "../auth/sign-up/sign-up.component";
import { AssignmentComponent } from "./assignment.component";
import { ExerciseComponent } from "./assingment-2/assingment-1/exercise/exercise.component";
import { DirectivesComponent } from "./assingment-3/directives/directives.component";
import { ComCommunicationComponent } from "./assingment-4/com-communication/com-communication.component";
import { ListViewComponent } from "./assingment-6/list-view/list-view.component";
import { TemplateDrivenFormComponent } from "./assingment-6/template-driven-form/template-driven-form.component";
import { PipeComponent } from "./assingment-7/pipe/pipe.component";
import { FirebaseCommunicationComponent } from "./assingment-8/firebase-communication/firebase-communication.component";
import { OrderListComponent } from "./order-List/order-list/order-list.component";

const routes: Routes = [
    {
      path: '',
      component: AssignmentComponent,
      children: [
        {path:'communication', component:ComCommunicationComponent},
  //  {path:'directives', canActivate:[AuthGuard],component:DirectivesComponent},
  {path:'directives', component:DirectivesComponent},
    {path:'dataBinding', component:ExerciseComponent},
    {path:'forms', component:TemplateDrivenFormComponent},
    {path:'veg', component:ListViewComponent},
    {path:'list', component:OrderListComponent},
    {path:'pipe', component:PipeComponent},
    {path:'firebase', component:FirebaseCommunicationComponent}

      ]
    }
  ];

// const routes:Routes=[
//     {path:'communication', component:ComCommunicationComponent},
//   //  {path:'directives', canActivate:[AuthGuard],component:DirectivesComponent},
//   {path:'directives', component:DirectivesComponent},
//     {path:'dataBinding', component:ExerciseComponent},
//     {path:'forms', component:TemplateDrivenFormComponent},
//     {path:'veg', component:ListViewComponent},
//     {path:'list', component:OrderListComponent},
//     {path:'pipe', component:PipeComponent},
//     {path:'firebase', component:FirebaseCommunicationComponent}
//   ];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssingmentRoutingModule {}
