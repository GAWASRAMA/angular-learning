import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirebaseCommunicationComponent } from './firebase-communication.component';

describe('FirebaseCommunicationComponent', () => {
  let component: FirebaseCommunicationComponent;
  let fixture: ComponentFixture<FirebaseCommunicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirebaseCommunicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirebaseCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
