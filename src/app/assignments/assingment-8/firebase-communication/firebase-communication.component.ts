import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {map} from 'rxjs/operators';
@Component({
  selector: 'app-firebase-communication',
  templateUrl: './firebase-communication.component.html',
  styleUrls: ['./firebase-communication.component.scss']
})
export class FirebaseCommunicationComponent implements OnInit {
  userForm: FormGroup;
  userData=[];
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.createForm();
    this.getALLData();
  }
createForm():void{
  this.userForm = new FormGroup({
    firstName: new FormControl('',Validators.required),
    lastName: new FormControl('',Validators.required),
    mobNo: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required)
  });
}
onSubmit(): void{
  console.log(this.userForm.value);
  this.http.post<any>('https://my-project-2bd9f-default-rtdb.firebaseio.com/posts.json',this.userForm.value)
  .subscribe(res=>{
    console.log(res);
    this.getALLData();
    this.userForm.reset();
  })
}
getALLData():void{
  this.http.get<any>('https://my-project-2bd9f-default-rtdb.firebaseio.com/posts.json')
  .pipe(map(res=>{
    const dataArray = [];
    for(const key in res){
      if(res.hasOwnProperty(key)){
        dataArray.push({...res[key], id:key});
      }
    }
    return dataArray;
  }))
  .subscribe(data=>{
    console.log(data);
    this.userData = data;
  })
}

onClear(): void{
  this.http.delete<any>('https://my-project-2bd9f-default-rtdb.firebaseio.com/posts.json')
  .subscribe(res=>{
    console.log(res);
    this.getALLData();
  })
}
}
