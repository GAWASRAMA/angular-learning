import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { ExerciseComponent } from './assingment-2/assingment-1/exercise/exercise.component';
import { DataBindingComponent } from './assingment-2/data-binding/data-binding.component';
import { DirectivesComponent } from './assingment-3/directives/directives.component';
import { GameControlComponent } from './assingment-4/game-control/game-control.component';
import { OddComponent } from './assingment-4/odd/odd.component';
import { FirebaseCommunicationComponent } from './assingment-8/firebase-communication/firebase-communication.component';
import { CustomPipePipe } from './pipe/custom-pipe.pipe';
import { PipeComponent } from './assingment-7/pipe/pipe.component';
import { OrderListComponent } from './order-List/order-list/order-list.component';
import { ListViewComponent } from './assingment-6/list-view/list-view.component';
import { TemplateDrivenFormComponent } from './assingment-6/template-driven-form/template-driven-form.component';
import { ComCommunicationComponent } from './assingment-4/com-communication/com-communication.component';
import { EvenComponent } from './assingment-4/even/even.component';
import { AssingmentRoutingModule } from './assignment-routing.module';
import { UserManageService } from './services/user-manage.service';
import { CommonModule } from '@angular/common';
import { AssignmentComponent } from './assignment.component';
import { HeaderComponent } from '../header/header/header.component';

@NgModule({
  declarations: [
    HeaderComponent,
    AssignmentComponent,
    ExerciseComponent,
    DataBindingComponent,
    DirectivesComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    ComCommunicationComponent,
    TemplateDrivenFormComponent,
    ListViewComponent,
    OrderListComponent,
    PipeComponent,
    CustomPipePipe,
    FirebaseCommunicationComponent,
  ],
  imports: [
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    AssingmentRoutingModule,
    SharedModule,
    CommonModule
  ],
  providers: [
    UserManageService
  ]
})
export class AssingmentModule {}
