import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private service:AuthService) { }

  ngOnInit(): void {
    this.service.user.subscribe(user=>{
      console.log(user);
    })
  }

  onLogout(): void{
    this.service.logout();
  }

}
